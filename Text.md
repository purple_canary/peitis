# Peitis

## O podjetju CNC CENTER PEITIS d.o.o.

Podjetje CNC CENTER PEITIS je družinsko podjetje, ki je nastalo na podlagi 25-letnih izkušenj na področju obdelave kovin.

Temeljna dejavnost podjetja je 
proizvodnja drobnih kovinskih izdelkov za področje avtomobilske 
industrije, gradbeništva, strojegradnje, pohištvene industrije, 
elektronike in navtike.

Našim poslovnim partnerjem lahko ponudimo
 kvalitetne izdelke, kratke dobavne roke, prilagodljivost njihovim 
zahtevam in primerne tržne cene.



## Über das Unternehmen CNC CENTER PEITIS D.O.O.

Das
 Unternehmen CNC CENTER PEITIS ist ein Familienunternehmen, das auf der 
Grundlage von 25 Jahren Erfahrung auf dem Gebiet der Metallverarbeitung 
gegründet wurde.

Die Haupttätigkeit des Unternehmens ist 
die Produktion von kleinen Metall-Produkten für die Automobilindustrie, 
Bauingenieurwesen, Maschinenbau, Möbelindustrie, Elektronik und Nautik.

Unseren Geschäftspartnern bieten wir 
hochwertige Produkte, kurze Lieferzeiten, Flexibilität, Anpassung an 
ihre Forderungen und marktgerechte Preise an.



**1990**

Poslujemo od

**128**

Poslovnih partnerjev

**412**

Narejenih prototipov

**8**

Zaposlenih



**1990**

Arbeiten von

**128**

Geschäftspartner

**412**

Gemacht Prototypen

**8**

Mitarbeiter





### Zagotavljanje kakovosti

V
 podjetju posvečamo pozornost uvedbi mednarodnih standardov kakovosti za
 izdelke. S proizvodnim programom  smo že od začetka prisotni na 
zahtevnem avtomobilskem trgu. Vsi  procesi so  načrtovani, izvajani in  
nadzorovani.

[![](http://peitis.si/wp-content/uploads/2014/09/SGS_ISO-9001_TCL_HR-300x293.jpg)](http://peitis.si/wp-content/uploads/2014/09/SGS_ISO-9001_TCL_HR.jpg)Zaupanje
 kupcev ohranjamo z visokimi standardi kakovosti. Podjetje ima vpeljan 
standard kakovosti  ISO 9001:2015,  ki je pokazatelj naše kakovosti in 
merilo uspešnosti. Z  implementacijo in uvajanjem izboljšav omogoča 
doseganje čedalje boljših rezultatov poslovanja.

[Politika](http://peitis.si/wp-content/uploads/2014/09/Politika-kakovosti-2017.pdf) [kako](http://peitis.si/wp-content/uploads/2014/09/Politika-kakovosti-2017.pdf)[vosti 2017](http://peitis.si/wp-content/uploads/2014/09/Politika-kakovosti-2017.pdf)



### Qualitätssicherung

Im
 Unternehmen achten wir auf die Einführung von internationalen 
Qualitätsstandards für die Produkte. Mit unserem Fertigungsprogramm sind
 wir von Anfang an am anspruchsvollen Automobilmarkt präsent. Alle 
Prozesse sind geplant, umgesetzt und kontrolliert.

Wir bewahren das Vertrauen der Kunden mit hohen Qualitätsstandards. 
Das Unternehmen führte den Qualitätsstandard ISO 9001/2008 ein, der ein 
Indikator für unsere Qualität und unseren Erfolgsmaßstab ist. Durch die 
Umsetzung und Einführung von Verbesserungen ermöglicht er die 
Verwirklichung immer besserer



Naše prednosti so:

– kvaliteta (qualität)

– kratki dobavni roki (kurze Liefertermine)

– hitre prilagoditve (schnelle Anpassung)

– konkurenčne cene (marktgerechte Preise)



## Vizija podjetja

V
 podjetju sledimo viziji postati ključni dobavitelj velikih 
proizvajalcev v avtomobilski industriji na področju izdelave drobnih 
aluminijastih komponent.

Temeljni cilj podjetja je zadovoljstvo 
naših kupcev,  ki zahtevajo visoko kvaliteto v čim krajšem času. Želimo,
 da je CNC CENTER PEITIS d.o.o. podjetje, ki bo dajalo ime kakovosti 
izdelkov.



## VISION DES UNTERNEHMENS

Im
 Unternehmen verfolgen wir die Vision ein wichtiger Lieferant von großen
 Herstellern in der Automobilindustrie im Bereich der Herstellung von 
kleinen Aluminium-Bauteilen zu werden.

Das grundlegende Ziel des Unternehmens 
ist die Zufriedenheit unserer Kunden, die hohe Qualität in der kürzest 
möglichen Zeit verlangen. Wir wollen, dass CNC CENTER PEITIS d.o.o. ein 
Unternehmen ist, das den Namen der Produktqualität trägt





## Naše storitve

Podjetje
 je specializirano za mehansko obdelavo aluminija in nerjavega jekla po 
vaših načrtih. Zaradi neposredne bližine tovarne aluminija IMPOL  imamo 
hiter dostop do različnih kvalitet aluminija in aluminijastih profilov. 
Proizvajamo razne vrste izdelkov in polizdelkov iz kovine in plastike.

Poleg rednih serij izdelkov se ukvarjamo tudi s proizvodnjo t.i. 
nultih serij oziroma izdelavo prototipnih izdelkov, tako enostavnih kot 
tudi kompleksnejših.



## Naše zmožnosti

Obdelava poteka na:

- CNC stružnicah
- CNC stružnih avtomatih
- CNC obdelovalnih centrih
- vrtalni stroji

Obseg našega strojnega parka nam omogoča:

- Struženje izdelkov iz palice do premera φ5mm- φ45mm
- Kosovna obdelava do φ200mm in dolžine 500mm
- Rezkanje velikosti kosov 300mm x 1000mm

## Unsere Dienstleistungen

Das
 Unternehmen ist spezialisiert auf die mechanische Bearbeitung von 
Aluminium und rostfreiem Stahl nach Ihren Plänen. Aufgrund der Nähe der 
Aluminiumfabrik IMPOL haben wir schnellen Zugriff auf die verschiedenen 
Qualitäten von Aluminium und Aluminium-Profile. Wir produzieren 
verschiedene Produkte und Halbzeuge aus Metall und Kunststoff.

Neben der regulären Serie von Produkten beschäftigen wir uns auch mit
 der Produktion der sogenannten Null-Serien oder der Herstellung von 
Prototypen, sowohl einfachen als auch komplexen.



## Unsere Kapazitäten

Verarbeitung erfolgt auf:

- CNC-Drehmaschinen
- CNC-Bearbeitungsmaschinen
- CNC-Bearbeitungszentren
- Bohrmaschinen

Der Umfang unseres Maschinenparks ermöglicht uns:

- das Drechseln der Produkte aus Stäben, φ5mm-φ45mm
- individuelle Verarbeitung, φ200mm und einer Länge von 500 mm
- das Fräsen von Stücken von 300 mm x 1000 mm





### Kontaktni podatki (Kontaktdetails)

**PEITIS | CNC center  
**  
**Naslov:** Špindlerjeva ulica 30, 2310 Slovenska Bistrica  
**Telefon:** +386 2 843 16 80  
**Fax:** +386 2 843 16 81

**W:** www.peitis.si  
**E-Mail:** info@peitis.si

### Zaposlitev

Za uspešno poslovanje, rast in razvoj družbe na konkurenčnem tržišču so potrebni motivirani in strokovno usposobljeni kadri.

V kolektivu želimo posameznike, ki so odprti do novih idej, so 
samostojni in angažirani,  ter so pripravljeni razvijati svoje znanje in
 sposobnosti.

Če ste motivirani, z voljo do dela in željo po napredovanju, Vas vabimo, da se nam pridružite.

Prijavo z življenjepisem pošljite na naslov:

**CNC CENTER PEITIS D.O.O.**

Špindlerjeva 30

2310 Slov. Bistrica

ali po elektronski pošti.

*Trenutno nimamo razpisani prostih delovnih mest.*
